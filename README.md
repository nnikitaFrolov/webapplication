# README #

### This is a training web application using Hibernate, Servlets, PostgreSQL ###

### How set up? ###

* Create database PostgreSQL 
* Create table, use WebApplication\src\main\resources\database\initDB.sql
* Change hibernate.cfg.xml (database properties), if you need this
* Run project using comand "mvn tomcat7:run" in terminal
* Сlick on the link: http://localhost:8088/MainPage