package com.nnikitafrolov.pmsystem.model;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Class that represents entity DeveloperPage.
 * Used as a base class for create object this entity.
 *
 * @author Nikita Frolov
 */
@Entity
@Table(name = "developers")
public class Developer implements Identified<Long>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String first_name;

    @Column(name = "last_name")
    private String last_name;

    @Column(name = "specialty")
    private String specialty;

    @Column(name = "experience")
    private int experience;

    @Column(name = "salary")
    private BigDecimal salary;

    public Developer() {
    }

    public Developer(String first_name, String last_name, String specialty, int experience, BigDecimal salary) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.specialty = specialty;
        this.experience = experience;
        this.salary = salary;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "DeveloperPage{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", specialty='" + specialty + '\'' +
                ", experience=" + experience +
                ", salary=" + salary +
                '}';
    }
}
