package com.nnikitafrolov.pmsystem.services;

import java.util.List;

/**
 * Service interface used as a based class for Services classes
 *
 * @param <T>  type of object that service handles
 * @param <PK> primary key
 * @author Nikita Frolov
 */
public interface Services<T, PK> {
    /**
     * @param key primary key
     * @return returns the object corresponding to the entry with the primary key 'key'
     */
    T getById(PK key);

    /**
     * Creates a new record corresponding to the item 'object'
     *
     * @param entity that will corresponding new record
     */
    void save(T entity);

    /**
     * Saves the state of the object in the database
     *
     * @param entity that will corresponding new record
     */
    void update(T entity);

    /**
     * Removes a record about an object from the database
     *
     * @param entity record about which removes
     */
    void remove(T entity);

    /**
     * @return returns all object
     */
    List<T> getAll();
}
