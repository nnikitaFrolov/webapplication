package com.nnikitafrolov.pmsystem.services;

import com.nnikitafrolov.pmsystem.controller.DeveloperController;
import com.nnikitafrolov.pmsystem.model.Developer;

import java.util.List;

public class DeveloperServices implements Services<Developer, Long> {
    private DeveloperController developerController;

    public DeveloperServices() {
        this.developerController = new DeveloperController();
    }

    @Override
    public Developer getById(Long key) {
        return developerController.getById(key);
    }

    @Override
    public void save(Developer entity) {
        developerController.save(entity);
    }

    @Override
    public void update(Developer entity) {
        developerController.update(entity);
    }

    @Override
    public void remove(Developer entity) {
        developerController.remove(entity);
    }

    @Override
    public List<Developer> getAll() {
        return developerController.getAll();
    }
}
