package com.nnikitafrolov.pmsystem.dao;

import com.nnikitafrolov.pmsystem.model.Identified;

import java.io.Serializable;
import java.util.List;

/**
 * A unified interface management persistent state of objects
 *
 * @param <T>  type object persistent that extends {@link Identified}
 * @param <PK> type primary key that extends {@link Serializable}
 * @author Nikita Frolov
 */
public interface GenericDao<T extends Identified<PK>, PK extends Serializable> {

    /**
     * Creates a new record corresponding to the item 'object'
     *
     * @param entity that will corresponding new record
     */
    void save(T entity);

    /**
     * @param key primary key
     * @return returns the object corresponding to the entry with the primary key 'key'
     */
    T getByPK(PK key);

    /**
     * Saves the state of the object in the database
     *
     * @param entity that will corresponding new record
     */
    void update(T entity);

    /**
     * @return returns a list of objects matching all records in the database
     */
    List<T> getAll();

    /**
     * Removes a record about an object from the database
     *
     * @param entity record about which removes
     */
    void delete(T entity);
}