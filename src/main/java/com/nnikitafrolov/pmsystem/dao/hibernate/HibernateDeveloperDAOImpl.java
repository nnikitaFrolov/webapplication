package com.nnikitafrolov.pmsystem.dao.hibernate;

import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.model.Developer;
import com.nnikitafrolov.pmsystem.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Developer},
 * that implement {@link GenericDao}
 *
 * @author Frolov Nikita
 */
public class HibernateDeveloperDAOImpl implements GenericDao<Developer, Long> {
    private SessionFactory sessionFactory;
    private static final String ALL_PRODUCT = "FROM Developer";

    public HibernateDeveloperDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    public void save(Developer entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        session.save(entity);
        transaction.commit();
        session.close();
    }

    @Override
    public Developer getByPK(Long key) {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        Developer entity = session.get(Developer.class, key);
        transaction.commit();
        session.close();
        return entity;
    }

    @Override
    public void update(Developer entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        session.merge(entity);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Developer> getAll() {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        List<Developer> entities = session.createQuery(ALL_PRODUCT, Developer.class).list();
        transaction.commit();
        session.close();
        return entities;
    }

    @Override
    public void delete(Developer entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        session.delete(entity);
        transaction.commit();
        session.close();
    }
}
