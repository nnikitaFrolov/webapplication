package com.nnikitafrolov.pmsystem.servlets;

import com.nnikitafrolov.pmsystem.model.Developer;
import com.nnikitafrolov.pmsystem.services.DeveloperServices;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class AllDevelopers extends HttpServlet {

    private DeveloperServices developerServices;

    public void init() {
        this.developerServices = new DeveloperServices();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter writer = response.getWriter();
        String title = "ListDeveloper";
        String docType = "<!DOCTYPE html>";

        List<Developer> developers = developerServices.getAll();
        StringBuilder listLinksOnDeveloper = new StringBuilder();
        for (Developer developer :
                developers) {
            listLinksOnDeveloper.append("<br><a href=\"/DeveloperPage?id=")
                    .append(+developer.getId()).append("\">").append(developer.getId())
                    .append(" | ").append(developer.getFirst_name()).append(" ")
                    .append(developer.getLast_name()).append("</a>");
        }

        writer.println(docType + "<html>" +
                "<head><title>" + title + "</title></head>\n" +
                "<body><h2>ListDeveloper:</h2>" +
                listLinksOnDeveloper +
                "<br><a href=\"/CreateDeveloper.html\">Create developer</a>"
                + "</body>" +
                "</html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
