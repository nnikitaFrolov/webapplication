package com.nnikitafrolov.pmsystem.servlets;

import com.nnikitafrolov.pmsystem.model.Developer;
import com.nnikitafrolov.pmsystem.services.DeveloperServices;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

public class ManipulationDeveloper extends HttpServlet{
    private DeveloperServices developerServices;
    private Long currentDeveloper;

    public void init() {
        this.developerServices = new DeveloperServices();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter writer = response.getWriter();
        String title = "ManipulationDeveloper";
        String docType = "<!DOCTYPE html>";

        currentDeveloper = Long.valueOf(request.getParameter("id"));
        Developer developer = developerServices.getById(currentDeveloper);
        writer.println(docType + "<html>" +
                "<head><title>" + title + "</title></head>\n" +
                "<body><h2>Developer: </h2>" +
                "<form action=\"ManipulationDeveloper?mode=1\" method=\"POST\">\n" +
                "    First name: <label>\n" +
                "    <input type=\"text\" name=\"first_name\" value=\"" + developer.getFirst_name() + "\">\n" +
                "</label>\n" +
                "    <br/>\n" +
                "    Last name: <label>\n" +
                "    <input type=\"text\" name=\"last_name\" value=\"" + developer.getLast_name() + "\">\n" +
                "</label>\n" +
                "    <br/>\n" +
                "    Specialty: <label>\n" +
                "    <input type=\"text\" name=\"specialty\" value=\"" + developer.getSpecialty() + "\">\n" +
                "</label>\n" +
                "    <br/>\n" +
                "    Experience: <label>\n" +
                "    <input type=\"text\" name=\"experience\" value=\"" + developer.getExperience() + "\">\n" +
                "</label>\n" +
                "    <br/>\n" +
                "    Salary: <label>\n" +
                "    <input type=\"text\" name=\"salary\" value=\"" + developer.getSalary() + "\">\n" +
                "</label>\n" +
                "    <input type=\"submit\" value=\"Save\">\n" +
                "</form>"
                + "</body>" +
                "</html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Developer developer = new Developer();
        developer.setFirst_name(request.getParameter("first_name"));
        developer.setLast_name(request.getParameter("last_name"));
        developer.setSpecialty(request.getParameter("specialty"));
        developer.setExperience(Integer.valueOf(request.getParameter("experience")));
        developer.setSalary(BigDecimal.valueOf(Long.parseLong(request.getParameter("salary"))));

        if (request.getParameter("mode") != null && Integer.parseInt(request.getParameter("mode")) == 1) {
            developer.setId(currentDeveloper);
            developerServices.update(developer);
        } else {
            developerServices.save(developer);
        }

        response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
        response.setHeader("Location", "http://localhost:8088/AllDevelopers");
    }

}
