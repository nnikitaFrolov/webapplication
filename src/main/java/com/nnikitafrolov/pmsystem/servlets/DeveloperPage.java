package com.nnikitafrolov.pmsystem.servlets;

import com.nnikitafrolov.pmsystem.model.Developer;
import com.nnikitafrolov.pmsystem.services.DeveloperServices;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DeveloperPage extends HttpServlet {
    private DeveloperServices developerServices;

    public void init() {
        this.developerServices = new DeveloperServices();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter writer = response.getWriter();
        String title = "Developer";
        String docType = "<!DOCTYPE html>";

        if (request.getParameter("mode") != null && Integer.parseInt(request.getParameter("mode")) == 1) {
            Developer developer = new Developer();
            developer.setId(Long.parseLong(request.getParameter("id")));
            developerServices.remove(developer);
            response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
            response.setHeader("Location", "http://localhost:8088/AllDevelopers");
            return;
        }

        Developer developer = developerServices.getById(Long.valueOf(request.getParameter("id")));

        writer.println(docType + "<html>" +
                "<head><title>" + title + "</title></head>\n" +
                "<body><h2>Developer:</h2>" +
                developer.toString() +
                "<br><a href=\"/ManipulationDeveloper?id=" + developer.getId() + "&mode=1\">Change" + "</a>" +
                "<br><a href=\"/DeveloperPage?id=" + developer.getId() + "&mode=1\">Remove" + "</a>" +
                "</body>" +
                "</html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
