package com.nnikitafrolov.pmsystem.controller;

import com.nnikitafrolov.pmsystem.dao.hibernate.HibernateDeveloperDAOImpl;
import com.nnikitafrolov.pmsystem.model.Developer;

import java.util.List;

/**
 * Controller that handles request connected with {@link Developer}
 *
 * @author Nikita Frolov
 */
public class DeveloperController implements Controller<Developer, Long> {
    private HibernateDeveloperDAOImpl developerDAO;

    public DeveloperController() {
        this.developerDAO = new HibernateDeveloperDAOImpl();
    }

    @Override
    public Developer getById(Long key) {
        return developerDAO.getByPK(key);
    }

    @Override
    public void save(Developer entity) {
        developerDAO.save(entity);
    }

    @Override
    public void update(Developer entity) {
        developerDAO.update(entity);
    }

    @Override
    public void remove(Developer entity) {
        developerDAO.delete(entity);
    }

    @Override
    public List<Developer> getAll() {
        return developerDAO.getAll();
    }
}
